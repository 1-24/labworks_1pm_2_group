#include "point.h"
#include <QPainter>

Point::Point()
    :Point(0,0)
{
}

Point::Point(int x, int y, int size, int thickness)
    :x(x), y(y), size(size), thickness(thickness)
{
}

int Point::getX() const
{
    return x;
}

void Point::setX(int newX)
{
    x = newX;
}

int Point::getY() const
{
    return y;
}

void Point::setY(int newY)
{
    y = newY;
}

int Point::getSize() const
{
    return size;
}

void Point::setSize(int newSize)
{
    size = newSize;
}

int Point::getThickness() const
{
    return thickness;
}

void Point::setThickness(int newThickness)
{
    thickness = newThickness;
}


void Point::draw(QPainter *painter)
{
    QPen pen(Qt::black);
    pen.setWidth(thickness);
    painter->setPen(pen);
    painter->drawLine(x, y - size, x, y + size);
    painter->drawLine(x - size, y, x + size, y);
}


