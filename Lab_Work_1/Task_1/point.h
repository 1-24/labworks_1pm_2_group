#ifndef POINT_H
#define POINT_H

class QPainter;

class Point
{
public:
    Point();
    Point(int x=0, int y=0, int size = 3, int thickness = 1);

    int getX() const;
    void setX(int newX);

    int getY() const;
    void setY(int newY);

    int getSize() const;
    void setSize(int newSize);

    int getThickness() const;
    void setThickness(int newThickness);

    void draw(QPainter *);

private:
    int x,y,size,thickness;
};

#endif // POINT_H
