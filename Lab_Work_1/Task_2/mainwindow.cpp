#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QPen>
#include <QPoint>
#include <QTimer>
#include <QMouseEvent>
#include "point.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    for(int i=0; i<points.size(); i++)
    {
        if(points[i])
        {
            points[i]->draw(&painter);
            if(i>0)
            {
                QPen pen(Qt::black);
                pen.setWidth(1);
                painter.setPen(pen);
                painter.drawLine(points[i-1]->getX(), points[i-1]->getY(), points[i]->getX(), points[i]->getY());
            }
        }
    }

}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    Point *p = nullptr;
    bool flag = false;
    for(int i=0; i<points.size(); i++)
    {
        QPoint point(event->x(),event->y());
        if((points[i]->distance2(point))<=25)
        {
            points.erase(points.begin()+i);
            delete(p);
            n--;
            if(!flag)   {flag = true;}
        }
    }
    if(!flag)
    {
        p = new Point(event->x(), event->y(), 3+n, (n<5 ? 1 : 2));
        if(p)
            points.push_back(p); n++;
    }
    repaint();
}

void MainWindow::update()
{
    repaint();
}

