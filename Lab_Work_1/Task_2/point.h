#ifndef POINT_H
#define POINT_H

class QPainter;
class QPoint;

class Point
{
public:
    Point();
    Point(int x, int y, int size = 3, int thickness = 1);

    int getX() const;
    void setX(int newX);

    int getY() const;
    void setY(int newY);

    int getSize() const;
    void setSize(int newSize);

    int getThickness() const;
    void setThickness(int newThickness);

    void draw(QPainter *);
    int distance2(const QPoint &point);

private:
    int x,y,size,thickness;
};

#endif // POINT_H
