#ifndef POINT_H
#define POINT_H

class QPainter;
class QPoint;
class QMouseEvent;

class Point
{
public:
    Point();
    Point(int x, int y, int size = 3, int thickness = 1, bool captured = false );

    int getX() const;
    void setX(int newX);

    int getY() const;
    void setY(int newY);

    int getSize() const;
    void setSize(int newSize);

    int getThickness() const;
    void setThickness(int newThickness);

    void draw(QPainter *);
    int distance2(const QPoint &point);

    bool getCaptured() const;
    void setCaptured(bool newCaptured);

private:
    int x,y,size,thickness;
    bool captured;
};

#endif // POINT_H
