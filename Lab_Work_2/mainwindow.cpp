#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QMouseEvent>
#include "rect.h"
#include <QDebug>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if(n>=2)
    {
        r->draw(&painter);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    switch(n)
    {
    case 0:
        tmp_x = event->x();
        tmp_y = event->y();
        n++;
        break;
    case 1:
        r = new Rect(tmp_x,tmp_y,event->x(),event->y());
        n++;
        break;
    default:
        QMessageBox msgBox;
        if(r->contains(event->x(),event->y()))
        {
           msgBox.setText("Попали!");
           msgBox.exec();
        }
        else
        {
            msgBox.setText("Не попали!");
            msgBox.exec();
        }
        break;
    }
    repaint();
}

