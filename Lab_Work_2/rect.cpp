#include "rect.h"
#include <QPainter>

Rect::Rect()
    :Rect(0,0,0,0)
{
}

Rect::Rect(int x1, int y1, int x2, int y2)
    :x1(std::min(x1,x2)), y1(std::min(y1,y2)), x2(std::max(x1,x2)), y2(std::max(y1,y2))
{
}

int Rect::getX1() const
{
    return x1;
}

void Rect::setX1(int newX1)
{
    x1 = newX1;
}

int Rect::getY1() const
{
    return y1;
}

void Rect::setY1(int newY1)
{
    y1 = newY1;
}

int Rect::getX2() const
{
    return x2;
}

void Rect::setX2(int newX2)
{
    x2 = newX2;
}

int Rect::getY2() const
{
    return y2;
}

void Rect::setY2(int newY2)
{
    y2 = newY2;
}

void Rect::draw(QPainter *painter)
{
    QPen pen(Qt::black);
    pen.setWidth(1);
    painter->setPen(pen);
    painter->drawRect(QRect(x1,y1,x2-x1,y2-y1));
}

bool Rect::contains(int x, int y)
{
    return((x>=x1)&&(x<=x2)&&(y>=y1)&&(y<=y2));
}
