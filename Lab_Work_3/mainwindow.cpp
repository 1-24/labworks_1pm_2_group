#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "rect.h"
#include <QMouseEvent>
#include <QPainter>
#include <QBrush>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    for(int num=2; num<rects.size();num++)
    {
        for(int i=0; i<rects.size(); i++)
        {
            if(rects[i])
            {
                if((rects[i]->getIntersection()==num))
                    rects[i]->draw(&painter);
            }
        }
    }
    for(int i=0; i<rects.size(); i++)
    {
        if(rects[i])
        {
            if((rects[i]->getIntersection()==0)||(rects[i]->getIntersection()==1))
                rects[i]->draw(&painter);
        }
    }



}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    Rect *r = nullptr;
    if(event->modifiers()==Qt::ControlModifier)
    {
        switch(n)
        {
        case 0:
            tmp_x = event->x();
            tmp_y = event->y();
            n++;
            break;
        case 1:
            r = new Rect(tmp_x, tmp_y, event->x(), event->y());
            rects.push_back(r);
            n--;
            break;
        }
    }
    else
    {
        int intersections = 0,x_1=0,y_1=0,x_2=10000,y_2=10000, num_same;
        bool same = false;
        for(int i=0; i<rects.size(); i++)
        {
            if((rects[i])&&(rects[i]->contains(event->x(),event->y())))
            {
                switch(rects[i]->intersect(&x_1,&x_2,&y_1,&y_2,&intersections))
                {
                case 1:
                    same = true;
                    num_same = i;
                    break;
                case 0:
                    same = false;
                    break;
                default:
                    break;
                }
            }   
        }
        qDebug() << "intersections: " << intersections;
        if((!same)&&(intersections>1))
        {
            r = new Rect(x_1,y_1,x_2,y_2);
            r->setIntersection(intersections);
            rects.push_back(r);
            qDebug() << "new" << intersections;
        }
        if(same)
        {
            rects[num_same]->setIntersection(intersections);
            qDebug() << "same";
        }
        qDebug() << "size: " << rects.size();
        qDebug() << "\n";
    }

    repaint();
}



