#include "rect.h"

#include <iostream>
#include <QBrush>
#include <QPainter>
#include <QDebug>

Rect::Rect()
    :Rect(0,0,0,0)
{
}

Rect::Rect(int x1, int y1, int x2, int  y2)
    :x1(std::min(x1,x2)),y1(std::min(y1,y2)), x2(std::max(x1,x2)), y2(std::max(y1,y2))
{
}

void Rect::setIntersection(int newIntersection)
{
    intersection = newIntersection;
}

int Rect::getIntersection() const
{
    return intersection;
}

void Rect::draw(QPainter *painter)
{

    switch(intersection)
    {
    case 0:
    case 1:
        painter->setBrush(Qt::transparent);
        break;
    case 2:
        painter->setBrush(Qt::red);
        break;
    case 3:
        painter->setBrush(Qt::yellow);
        break;
    case 4:
        painter->setBrush(Qt::green);
        break;
    case 5:
        painter->setBrush(Qt::blue);
        break;
    case 6:
        painter->setBrush(Qt::black);
        break;
    default:
        break;

    }
    if(intersection>6) {painter->setBrush(Qt::black);}
    painter->drawRect(QRect(x1,y1,x2-x1,y2-y1));


}

bool Rect::contains(int x, int y)
{
    bool result = 0;

   if((x>=x1)&&(x<=x2)&&(y>y1)&&(y<=y2))
       result=true;

   return result;
}

int Rect::intersect(int *x_1, int *x_2, int *y_1, int *y_2, int *intersections)
{
    if((x1==*x_1) && (x2==*x_2)&&(y1==*y_1)&&(y2==*y_2))
    {
        return 1;
    }
    else
    {
        if(x1>*x_1) {*x_1 = x1;}
        if(x2<*x_2) {*x_2 = x2;}
        if(y1>*y_1) {*y_1 = y1;}
        if(y2<*y_2) {*y_2 = y2;}

        if(intersection>1) {return -1;}
        else {*intersections=*intersections+1; return 0;}
    }
}
















