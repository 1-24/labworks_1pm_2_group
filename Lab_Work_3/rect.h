#ifndef RECT_H
#define RECT_H


class QPainter;
class Rect
{
public:
    Rect();
    Rect(int x1, int y1, int x2, int y2);

    int getIntersection() const;
    void setIntersection(int newIntersection);

    void draw(QPainter *painter);
    bool contains(int x, int y);
    int intersect(int *x_1, int *x_2, int *y_1, int *y_2, int *intersections);

private:
    int x1, x2, y1, y2, intersection=0;
};

#endif // RECT_H
