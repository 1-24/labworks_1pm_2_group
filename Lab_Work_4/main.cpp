#include <QCoreApplication>
#include <QDebug>
#include "mytime.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //время начала 5 пары
    MyTime h1(15, 20);
    //время конца 5 пары
    MyTime h2(16, 50);
    qDebug() << h2 - h1;
    (h1+90).print();

    return a.exec();
}
