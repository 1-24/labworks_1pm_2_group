#include "mytime.h"
#include <QDebug>

MyTime::MyTime()
    :MyTime(0,0)
{
}

MyTime::MyTime(int h, int m)
{
    this->h = h%24;
    this->m = m;

}

int MyTime::operator -(MyTime o)
{
   if((h*60+m-o.h*60-o.m)>0)
       return (h*60+m-o.h*60-o.m);
   else
       return -(h*60+m-o.h*60-o.m);
}

MyTime MyTime::operator +(int min)
{
    return MyTime(h + min/60, m + min%60);
}

void MyTime::print()
{
    if(h<10)
    {
        if(m<10) {qDebug("0%d:0%d",h,m);}
        else {qDebug("0%d:%d",h,m);}
    }
    else
    {
        if(m<10) {qDebug("%d:0%d",h,m);}
        else {qDebug("%d:%d",h,m);}
    }
}


