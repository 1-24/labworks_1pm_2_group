#ifndef MYTIME_H
#define MYTIME_H


class MyTime
{
public:
    MyTime();
    MyTime(int h, int m);

    int operator -(MyTime o);
    MyTime operator +(int min);
    void print();
private:
    int h,m;
};

#endif // MYTIME_H
